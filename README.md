# "Détection de Ruptures en Ligne avec le Test GLR Bernoulli, et Application aux Bandits Multi-Bras Stationnaires Par Morceaux"
This repository contains the LaTeX code of a research article written by [Lilian Besson](https://perso.crans.org/besson/) and [Emilie Kaufmann](http://chercheurs.lille.inria.fr/ekaufman/research.html), entitled "Détection de Ruptures en Ligne avec le Test GLR Bernoulli, et Application aux Bandits Multi-Bras Stationnaires Par Morceaux".

Our article ([see this PDF](https://perso.crans.org/besson/articles/BK__GRETSI__2019.pdf)) was sent to the [MOTION 2019 workshop](https://sites.google.com/view/wcncworkshop-motion2019) at the [GRETSI](http://gretsi.fr/colloque2019/) conference in March 2019.

- PDF : [BK__GRETSI__2019.pdf](https://hal.inria.fr/hal-XXX/document)
- HAL notice : [BK__GRETSI__2019](https://hal.inria.fr/hal-XXX/)
- BibTeX : [BK__GRETSI__2019.bib](https://hal.inria.fr/hal-XXX/bibtex)
- Source code and documentation: [on Bitbucket](https://SMPyBandits.GitHub.io/NonStationaryBandits.html)

[![Published](https://img.shields.io/badge/Published%3F-yes-green.svg)](https://hal.inria.fr/hal-XXX)
[![Maintenance](https://img.shields.io/badge/Maintained%3F-done-blue.svg)](XXX)
[![Ask Me Anything !](https://img.shields.io/badge/Ask%20me-anything-1abc9c.svg)](https://bitbucket.org/lbesson/ama)

> [I (Lilian Besson)](https://perso.crans.org/besson/) have [started my PhD](https://perso.crans.org/besson/phd/) in October 2016, and this is a part of my **on going** research in 2019.

----

## Abstract

In the context of online statistical learning, we propose a new algorithm for the detection of breakpoints, based on the generalized likelihood test and the Kullback-Leibler distance for Bernoulli distributions.
We give an analysis of its properties in finite time, unlike the state of the art which is interested in asymptotic properties.
Our results show the effectiveness of our algorithm, which applies to any bounded support distributions.
We also propose an application of this algorithm to the problems of multi-armed stationary bandits per piece, based on the index strategy \klUCB{}, and we show that the strategy obtained is numerically effective and obtains guarantees comparable to the best state of the art strategies.

## Thanks
This work is partially supported by Région Bretagne, France, the French Ministry of Higher Education and Research (MENESR), by École Normale Supérieure de Paris-Saclay.

----

## :scroll: License ? [![GitHub license](https://img.shields.io/github/license/Naereen/badges.svg)](https://gitlab.inria.fr/lbesson/gretsi-2019---detection-de-ruptures-en-ligne-avec-le-test-glr-bernoulli-et-application-aux-bandits-multi-bras-stationnaires-par-morceaux/blob/master/LICENSE)
[MIT Licensed](https://lbesson.mit-license.org/) (file [LICENSE](LICENSE)).

© 2019 [Lilian Besson](https://perso.crans.org/besson/) and [Emilie Kaufmann](http://chercheurs.lille.inria.fr/ekaufman/research.html)

[![Published](https://img.shields.io/badge/Published%3F-yes-green.svg)](https://hal.inria.fr/hal-XXX)
[![Maintenance](https://img.shields.io/badge/Maintained%3F-done-blue.svg)](https://gitlab.inria.fr/lbesson/gretsi-2019---detection-de-ruptures-en-ligne-avec-le-test-glr-bernoulli-et-application-aux-bandits-multi-bras-stationnaires-par-morceaux/)
[![Ask Me Anything !](https://img.shields.io/badge/Ask%20me-anything-1abc9c.svg)](https://bitbucket.org/lbesson/ama)
[![Analytics](https://ga-beacon.appspot.com/UA-38514290-17/bitbucket.org/lbesson/gretsi-2019---detection-de-ruptures-en-ligne-avec-le-test-glr-bernoulli-et-application-aux-bandits-multi-bras-stationnaires-par-morceaux/README.md?pixel)](XXX)
[![ForTheBadge uses-badges](http://ForTheBadge.com/images/badges/uses-badges.svg)](http://ForTheBadge.com)
[![ForTheBadge uses-git](http://ForTheBadge.com/images/badges/uses-git.svg)](https://GitHub.com/)
[![forthebadge made-with-latex](https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg)](https://www.latex-project.org/)
[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://perso.crans.org/besson/)
